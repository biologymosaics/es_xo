module.exports = {
    "env": {
        "browser": true,
        // "es6": true,
        "node": true,
    },
    "extends": [
		"eslint:recommended"
	],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 8,
        "sourceType": "module"
        // "sourceType": "module"
    },
    "rules": {
        // Yes!!!
        "no-dupe-keys": 2,
		"no-extend-native": 2,


        // "no-prototype-builtins": 0,
        // "no-extra-semi": 0,

        // "no-unused-vars": 1
        // "no-unused-vars": 0
        // "indent": ["error", "tab"]

		// "node/exports-style": ["error", "module.exports"],
        // "node/file-extension-in-import": ["error", "always"],
        // "node/prefer-global/buffer": ["error", "always"],
        // "node/prefer-global/console": ["error", "always"],
        // "node/prefer-global/process": ["error", "always"],
        // "node/prefer-global/url-search-params": ["error", "always"],
        // "node/prefer-global/url": ["error", "always"],
        // "node/prefer-promises/dns": "error",
        // "node/prefer-promises/fs": "error"
    },
    "overrides": [{
        "files": [
            "*.vow.js"
        ],
        "globals": {
            "guarantee": true
        }
    }]
};
