
/*
	Warning

	1 -> Next Called More than once within an iteration, Call after 1st call ignored
			* second "next()" could be in last iteration
			* second "next()" could be in setTimeout that is called after last iteration

			* safe syntax is "return next();" or "next(); return;"
				* but first has one less semicolon
*/

function Board() {};
Board.prototype.macros = function(m) {
    const board = this;

    function Macros(macros) {
        board.list = macros.list;
        board.each = macros.each;
        board.success = macros.success;
		board.warning = macros.warning;
		board.reverse = macros.reverse;

        board.iterator = board.reverse ? board.list.length : -1;
        /*
        	Could do:
        		type / constructor checking here
        */

		board.next();
    }

    return new Macros(m);
};
Board.prototype.next = function() {
    const board = this;

	if (board.reverse) {
		if (board.iterator === 0) {
			board.success();
			return false;
		}
	}
	else {
		if (board.iterator === board.list.length - 1) {
	        board.success();
	        return false;
	    }
	}



    /*
    	Could do:
    		if (board.iterator >= board.list.length) {
    			failure()
    		}
    */

	function Next({ index }) {
		this.called = false;
		this.fn = function() {
			if (this.called) {
				board.warning(1, {
					index
				});
			}
			else {
				this.called = true;
				board.next();
			}
		}
	}

	if (board.reverse) {
		board.iterator--;
	}
	else {
		board.iterator++;
	}

	setTimeout(() => {
		const next = new Next({
			index: board.iterator
		});

	    board.each(
	        board.list[
	            board.iterator
	        ],
	        board.iterator,
	        () => {
				next.fn();
	        }
	    );
	}, 0);
};

const presets = {
	reverse: false,
	warning: (message, details) => {
		throw new Error(JSON.stringify({
			message,
			details
		}, null, 4));
	}
}

module.exports = require("./../start.xo")(presets, (settings) => {
	const board = new Board();
	return board.macros(settings);
});
