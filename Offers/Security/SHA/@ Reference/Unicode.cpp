
/*
	https://tools.ietf.org/html/rfc3629

	Char. number range  |        UTF-8 octet sequence
      (hexadecimal)    |              (binary)
   --------------------+---------------------------------------------
   0000 0000 to 0000 007F | 0xxxxxxx
   0000 0080 to 0000 07FF | 110xxxxx 10xxxxxx
   0000 0800 to 0000 FFFF | 1110xxxx 10xxxxxx 10xxxxxx
   0001 0000 to 0010 FFFF | 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx


	0xxxxxxxx -> OOOOOOOOO to OXXXXXXXX
		2^7 Possibilities -> 128

		OOOOOOOOO -> 0... ? ->
		OOOOOOOOX ->
		OOOOOOOXO
		...
		OXXXXXXXX


		So 0 to 127
		Or 00 to 7F

	110xxxxx 10xxxxxx ->
		2^(5 + 6) Possibilities -> 2048 Possibilities

		XXOOOOOOO
*/
