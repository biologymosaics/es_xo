
/*
	const bracket = {
		a: true
	};

	bracketAdd({
		bracket,
		place: [ 'a', 'b', [ 3 ] ],
		value: null,
		success: () => {

		}
	});

	{
		a: {
			b: [ undefined, undefined, undefined, null ]
		}
	}
*/

/*
	const bracket = {
		a: true
	};

	bracketAdd({
		bracket,
		place: [ 'a', 'b', [ 3 ] ],
		strandEvo: {
			catalyze: catalyzeStrandEvo,
			evo: []
		},
		success: () => {

		}
	});

	{
		a: {
			b: [ undefined, undefined, undefined, null ]
		}
	}
*/

/*
	////////////////////////////
		Failures
	////////////////////////////

	1. Unexpected type found in "place"
	2. Unexpected type found in last index of "place"

	3. Object key not writable

	4. Place must have at least on entry.
*/

/*
	Examples:
		{ a: { a: true, b: 'A B C', c: { a: false } } }
		{ a: true, b: 'A B C', c: { a: false } }
*/

const presets = {
	// overwrite: true,
	failure: (message, details) => {
		throw new Error(JSON.stringify({
			message,
			details
		}, null, 2));
	}
};

const readList = require("./../list/read.xo");

module.exports = function(macros) {
	const { bracket, place, value, strandEvo, success } = macros;
	const failure = macros.failure || presets.failure;

	let current = bracket;

	let strandEvoValue = false;
	if (typeof strandEvo === "object") {
		strandEvoValue = true;
	}

	if (place.length === 0) {
		failure(4, {
			macros
		});
		return;
	}

	readList({
		list: place,
		each: function(partial, i, next) {
			/*
				Last partial in list
			*/
			if (i === place.length - 1) {
				if (
					typeof partial === "string" ||
					typeof partial === "number"
				) {
					if (strandEvoValue) {
						strandEvo.catalyze({
							strand: current[ partial ],
							evo: strandEvo.evo,
							success: ({ mStrand }) => {
								current[ partial ] = mStrand;
								if (current[ partial ] === mStrand) {
									next();
								}
								else {
									failure(3, {
										macros
									});
								}
							}
						});
					}
					else {
						current[ partial ] = value;
						if (current[ partial ] === value) {
							next();
						}
						else {
							failure(3, {
								macros
							});
						}
					}
				}
				else if (Array.isArray(partial)) {
					const [ index ] = partial;

					if (strandEvoValue) {
						strandEvo.catalyze({
							strand: current[ partial ],
							evo: strandEvo.evo,
							success: ({ mStrand }) => {
								current[ index ] = mStrand;
								if (current[ partial ] === mStrand) {
									next();
								}
								else {
									failure(3, {
										macros
									});
								}
							}
						});
					}
					else {
						current[ index ] = value;
						if (current[ index ] === value) {
							next();
						}
						else {
							failure(3, {
								macros
							});
						}
					}
				}
				else {
					failure(2, {
						macros
					});
				}
			}
			else {
				if (
					typeof partial === "string" ||
					typeof partial === "number" ||
					Array.isArray(partial)
				) {
					/*
						check that it modifies arrays correctly!!!
					*/

					/*
						e.g.
						[ 'a', 'b', [ 3 ] ]
					*/
					if (Array.isArray(place[i + 1])) {
						if (!Array.isArray(current[ partial ])) {
							current[ partial ] = [];
						}

						current = current[ partial ];
						next();
					}
					// else if () { // is Already an Object?
					//
					// }
					else {
						if (
							typeof current[ partial ] === "object" &&
							current[ partial ] !== null
						) {
							if (current[ partial ].constructor === Object) {
								// already declared?
								// so this not necessary
								// current[ partial ] = {};
							}
							else {
								current[ partial ] = {};
							}
						}
						else {
							current[ partial ] = {};
						}

						current = current[ partial ];
						next();
					}
				}
				else {
					failure(1, {
						place,
						partial,
						macros
					});
				}
			}
		},

		// called automatically if !(list.length >= 1)
		success: function() {
			success();
		}
	});
};
