
/*
	Needs Deep Equality Tests
*/

// mocha '**/*.broma.xo' --recursive --reporter spec -g "bracket mimic"

const mimic = require("./mimic.xo");
const strictEqualityCheck = require("./strict-equality-check.aux.xo");

describe("bracket mimic", () => {
	describe("original is unmodified", () => {
		it("one level, original unmodified", (done) => {
			const a = {
				a: true
			}

			const b = mimic(a);
			b.a = false;

			strictEqualityCheck(
				[a.a, true]
			);

			done();
		});
	});

	it("object:Arrays are equal", (done) => {
		const a = {
			a: [ 1, 2, 3 ]
		}

		const b = mimic(a);

		strictEqualityCheck(
			[ a.a.constructor, Array ],
			[ b.a.constructor, Array ],

			[ a.a[0], b.a[0] ],
			[ a.a[1], b.a[1] ],
			[ a.a[2], b.a[2] ]
		);

		done();
	});

	it("object:Dates are equal", (done) => {
		const a = {
			a: new Date()
		}

		const b = mimic(a);

		strictEqualityCheck(
			[ a.a.constructor, Date ],
			[ a.a, b.a ],
		);

		done();
	});

	it("multi-check", (done) => {
		const a = {
			a: new String("asdf"),
			b: new Object({
				hello: "there"
			}),
			c: new Date(),
			d: new RegExp(),

			zzz: {
				a: new String("asdf"),
				b: new Object({
					hello: "there"
				}),
				c: new Date(),
				d: new RegExp(),
				e: new Promise((resolve, reject) => {
					setTimeout(() => {
						resolve(true);
					}, 0);
				})
			}
		}

		const b = mimic(a);
		b.b = false;

		strictEqualityCheck(
			[ a.a[0], "a" ],
			[ a.a[1], "s" ],
			[ a.a[2], "d" ],
			[ a.a[3], "f" ],

			[ a.b.constructor, Object ],
			[ a.b.hello, "there" ],

			[ a.c.constructor, Date ],
			[ a.c, b.c ],

			[ a.d.constructor, RegExp ],

			[ a.zzz.a.constructor, String ],
			[ a.zzz.b.constructor, Object ],
			[ a.zzz.c.constructor, Date ],
			[ a.zzz.e.constructor, Promise ],
		);

		done();
	});
});
