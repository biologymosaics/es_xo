


describe ("Queue", () => {
	it ("Works", (done) => {
		const Queue = require ("../index.js") ({
	        poll_interval: 100
	    });

		var Predetermined_result = ["B1", "B2", "A1", "A2", "A3"];
		var Actual_result = [];

	    Queue.start ();

		// A is the Namespace
		// 	Only one function in the namespace can be run at a time
	    Queue.add ("A", ({ Conclude }) => {
	        setTimeout (() => {
	            console.log ("A1", new Date ());

				Actual_result.push ("A1");

	            Conclude ();
	        }, 1000);
	    });

		Queue.add ("A", ({ Conclude }) => {
	        setTimeout (() => {
				Actual_result.push ("A2");

	            console.log ("A2", new Date ());
	            Conclude ();
	        }, 10);
	    });

		Queue.add ("A", ({ Conclude }) => {
			setTimeout (() => {
				Actual_result.push ("A3");

				console.log ("A 3", new Date ());

				Queue.stop();
				if (Predetermined_result.join() === Actual_result.join ()) {
					done ();
				}
				else {
					throw null;
				}

				Conclude ();
			}, 100);
		});

		Queue.add ("B", ({ Conclude }) => {
	        setTimeout(() => {
				Actual_result.push ("B1");

				console.log ("B 1", new Date ());

	            Conclude ();
	        }, 10);
	    });

		Queue.add ("B", ({ Conclude }) => {
	        setTimeout(() => {
				Actual_result.push ("B2");

				console.log ("B 2", new Date ());

	            Conclude ();
	        }, 100);
	    });

		// need to call this or setInterval will repeat forever
		// Queue.stop ();
	});

	it ("Works with internal namespaces", (done) => {
		const Queue = require ("../index.js") ({
			poll_interval: 1
		});

		var Predetermined_result = ["A1", "A2", "A3", "A4"];
		var Actual_result = [];

		Queue.start ();

		var Namespace = Queue.Available ();
		Namespace.add (({ Conclude }) => {
			console.log ("Internal A");

			Actual_result.push ("A1");

			Conclude ();
		});

		Namespace.add (({ Conclude }) => {
			Actual_result.push ("A2");
			console.log ("A2");

			setTimeout (() => {
				Conclude ();
			}, 200);
		});

		Namespace.add (({ Conclude }) => {
			Actual_result.push ("A3");
			console.log ("A3");

			Conclude ();
		});

		Namespace.add (({ Conclude }) => {
			Actual_result.push ("A4");
			console.log ("A4");

			Conclude ();

			Queue.stop ();

			console.log ("?????");

			if (Predetermined_result.join () === Actual_result.join ()) {
				done ();
			}
			else {
				console.log ("Nope");
				throw null;
			}
		});

	});
});
