/*
	plan:

	produceBracketEvo({
		brackets: [
			<bracket A>,
			<bracket B>
		],
		success: ({ evo }) => {

		},
		progress: () => {

		},
		failure: () => {

		}
	});
*/

/*
	Bracket can be {} or []

	Even though...

		Object
			Array
*/

/*
	//////////////////////////////////////////
		Progress
	//////////////////////////////////////////


*/

/*
	//////
		Failures
	////////////

	1: not sure how to determine evo of two brackets or sub-brackets

	2: At object evo determination, however no constructor found on bracketA
	3: At object evo determination, however no constructor found on bracketB

	4: bracketA is of an unknown constructor

	99: Array Evo Not implemented
*/

/*
	//////////////////////////////////////////
		Evo Signifiers
	//////////////////////////////////////////

	1: modified
		[ A, B ]

	11: string modified
		[ forward strand evo, backward strand evo ]

	2: added
		[ B ]

	3: removed
		[ A ]

	4: same
		[ A, B ]
*/

/*
	//////////////////////////////////////////
		Evo Key Constructor Signifiers
	//////////////////////////////////////////




	Object Key <- if typeof key !== "object"
			[{
				0: true
			}, {
				0: true
			}],
			// [ [ 4, [ "0" ], [ true, true ] ] ]


	if typeof key === "object" && key.constructor === "Array"

		Array Key <- key[i][0] === 1

			[
				b: [ true ],
				b: [ true ]
			],

			evo = [
				[
					4,
					[ [ 0 ] ],
					[ true, true ]
				]
			]

	Question:
	if (
		typeof key === "object" &&
		key.constructor === "Array" &&
		key.length === 2
	)
*/
