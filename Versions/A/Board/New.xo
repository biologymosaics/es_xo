

/*
	failures

	1 -> id already registered

	31 -> Unknown Gate Found in Check Cycle
*/

/*
	progress events

	1 -> board started
	2 -> board stopped
	3 -> task added

	4 -> check started
		9 -> Checking task

		18 -> Nothing in possible Object
		20 -> AND Gate found
		22 -> Component that And Gate depends on failed, will never be able to start
		28 -> Component that And Gate depends on is not status success

		40 -> Check Cycle Completed

	45 -> Task Completed Successfully
*/

/*
	status codes

	1 -> added
	2 -> started

	3 -> completed successfully
	4 -> failed (won't stop the board)

	plan:
	5 -> repeating
		in which case
		{
			status: 4,
			repeat: 6 // # of the current repeat
		}
*/

const combineBrackets = require("./../bracket/combine.xo");
const readList = require("./../list/read.xo");
const Processor = require("./../Processor/New.xo");

const hasOwnProperty = (obj, prop) => {
	return Object.prototype.hasOwnProperty.call(obj, prop);
};

const presets = {
	failure: (message, details) => {
		throw new Error(JSON.stringify({
			message,
			details
		}, null, 2));
	}
}

const Task = require("./internal/Task.xo");

function Internal() {
	/*
		List of all Ids declared

		{
			status:
		}

		see "status codes"
	*/
	this.all = {};


	/*
		List of components still waiting to start

		gate: {
			id:
			details:
		}

		gate ids:
			1 -> AND Gate
	*/
	this.possible = {};

	this.processor = Processor({
		capacity: 1,
		poll_interval: 1
	});
}
Internal.prototype = combineBrackets(Internal.prototype, {
	init: function() {
		const internal = this;

		function Macros(macros) {
			const external_failure = macros.failure || presets.failure;
			internal.failure = (message, details) => {
				this.stop();
				external_failure(message, details);
			};

			internal.progress = macros.progress || function() {};

			// internal.progress = (message, details) => {
			// 	console.log("progress:", message, details);
			// }
		}
		Macros.prototype = combineBrackets(Macros.prototype, {
			add: function(components) {
				// console.log("Add called!!!!!");

				// console.log(internal.processor.getQueue())

				internal.processor.add(({ complete }) => {
					readList({
						list: components,
						each: function(component, index, next) {
							internal.add(component, () => {
								next();
							});
						},

						// called automatically if !(list.length >= 1)
						success: function() {
							complete();
							internal.check();
						}
					});
				});

				//
				// setInterval(() => {
				// 	console.log(internal.processor.getQueue())
				// }, 200);
			},
			start: function() {
				internal.processor.start();
				internal.progress(1);
			},
			stop: function() {
				internal.processor.stop();
				internal.progress(2);
			}
		});

		return Macros;
	},

	add: function(params, next) {
		// console.log("adding", params)

		const internal = this;
		const {
			id,
			fn
		} = params;

		let gate = null;

		if (hasOwnProperty(internal.all, id)) {
			internal.failure(1, {
				id
			});
			return;
		}

		internal.all[ id ] = {
			status: 1
		};
		internal.possible[ id ] = {
			fn
		};

		/*
			has any gates
		*/
		if (hasOwnProperty(params, "and")) {
			internal.possible[ id ].gate = {
				id: 1,
				details: params.and
			};
		}

		internal.progress(3, {
			id
		});

		next();
	},

	setStatus: function(id, num) {
		// console.log(id, "setting status", num);
		this.all[ id ].status = num;
	},

	/*
		plan:
			called on:
				* add
				* task success
	*/
	check: function() {
		const internal = this;
		internal.processor.add(({ complete }) => {
			const possible = Object.keys(internal.possible);

			internal.progress(4, {
				possible,
				all: internal.all,
			});

			if (possible.length === 0) {
				internal.progress(18);
				complete();
				return;
			}

			// console.log("checking", possible)

			readList({
				list: possible,
				each: function(id, index, next) {
					const component = internal.possible[ id ];
					// console.log(id, "checking id", component, internal.all);

					internal.progress(9, {
						id
					});

					// if (internal.all[ id ].status === 1) {
					//
					// }

					if (hasOwnProperty(component, "gate")) {
						if (component.gate.id === 1) { // And Gate Found
							internal.progress(20, {
								id
							});

							const { details } = component.gate;
							for (let i = 0; i <= details.length - 1; i++) {
								const waiting_on_id = details[ i ];

								/*
									check next possibility if:
								*/
								// there isn't a task with the id specified in the AND gate.
								// task could be added later...
								if (!hasOwnProperty(internal.all, waiting_on_id)) {
									next();
									return;
								}

								const { status } = internal.all[ waiting_on_id ];

								// task that is waiting on failed
								if (status === 4) {
									// remove this component from possible components
									delete internal.possible[ id ];

									internal.progress(22, {
										waiting_on_id,
										component
									});
									return;
								}

								// task that is waiting on is not a success
								// 3 -> success
								if (status !== 3) {
									internal.progress(28, {
										waiting_on_id,
										component
									});

									next();
									return;
								}
							}


							/*
								No gates found, task can start
							*/
							delete internal.possible[ id ];
							internal.setStatus(id, 2);

							const task = Task({
								id,
								success: ({ id: task_id }) => {
									internal.progress(45, {
										id: task_id
									});

									internal.setStatus(task_id, 3);

									internal.check();
								}
							});

							// console.log(id, "starting #");
							component.fn(task);
							next();
						}
						else {
							internal.failure(31);
							return;
						}
					}
					else {
						if (internal.all[ id ].status === 1) {
							delete internal.possible[ id ];
							internal.setStatus(id, 2);

							const task = Task({
								id,
								success: ({ id: task_id }) => {
									internal.progress(45, {
										id: task_id
									});

									internal.setStatus(task_id, 3);

									internal.check();
								}
							});


							// console.log(id, "starting %");
							component.fn(task);
							next();
						}
						else {
							next();
						}
					}
				},

				// called automatically if !(list.length >= 1)
				success: function() {
					internal.progress(40);
					complete();
				}
			});
		});
	}
});

module.exports = function(macros = {}) {
	var internal = new Internal();
	var Macros = internal.init()
	return new Macros(macros);
}
