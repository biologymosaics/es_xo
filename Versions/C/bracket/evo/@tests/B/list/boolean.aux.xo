
const produceBracketEvo = require("./../../../produce.xo");
const catalyzeBracketEvo = require('./../../../catalyze.xo');
const strictEqualityCheck = require("./../../../../strict-equality-check.aux.xo");

const tests = {
	boolean: {
		equivalent: [{
			name: "boolean and boolean, true",
			brackets: [ true, true ],
			check: ({ mBracket }) => {
				return [
					[ mBracket, true ]
				];
			}
		},{
			name: "boolean and boolean, false",
			brackets: [ false, false ],
			check: ({ mBracket }) => {
				return [
					[ mBracket, false ]
				];
			}
		}],
		["non-equivalent"]: [{
			name: "boolean and boolean, true and false",
			brackets: [ true, false ],
			check: ({ mBracket }) => {
				return [
					[ mBracket, false ]
				];
			}
		},{
			name: "boolean and boolean, false and true",
			brackets: [ false, true ],
			check: ({ mBracket }) => {
				return [
					[ mBracket, true ]
				];
			}
		}]
	}
}

describe("boolean %", () => {
	describe("equivalent", () => {
		const _tests = tests.boolean.equivalent;

		for (let i = 0; i <= _tests.length - 1; i++) {
			const { name, brackets, check } = _tests[i];
			const [ original, next ] = brackets;

			it(name, (done) => {
				produceBracketEvo({
					brackets: [ original, next ],
					success: ({ evo }) => {
						catalyzeBracketEvo({
							bracket: original,
							evo,
							success: ({ mBracket }) => {
								strictEqualityCheck.apply({},
									check({ mBracket })
								);

								done();
							}
						});
					}
				});
			});
		}
	});

	describe("non-equivalent", () => {
		const _tests = tests.boolean["non-equivalent"];

		for (let i = 0; i <= _tests.length - 1; i++) {
			const { name, brackets, check } = _tests[i];
			const [ original, next ] = brackets;

			it(name, (done) => {
				produceBracketEvo({
					brackets: [ original, next ],
					success: ({ evo }) => {
						catalyzeBracketEvo({
							bracket: original,
							evo,
							success: ({ mBracket }) => {
								strictEqualityCheck.apply({},
									check({ mBracket })
								);

								done();
							}
						});
					}
				});
			});
		}
	});
});
