

/*
	plan:

	catalyzeBracketEvo({
		bracket: {},
		evo: [],
		success: ({ mBracket }) => {

		}
	});

	* doesn't modify original "bracket"
		* write an immutability check
*/

/*
	//////
		Failures
	////////////

	1: unable to determine key
*/
const readList = require("./../../list/read.xo");
const mimic = require("./../mimic.xo");
const bracketAdd = require("./../add.xo");
const bracketRemove = require("./../remove.xo");
const catalyzeStrandEvo = require("./../../strand/evo/catalyze.xo");

const presets = {
	failure: (message, details) => {
		throw new Error(JSON.stringify({
			message,
			details
		}, null, 2));
	}
};

const isReferenceable = (bracket) => {
	if (typeof bracket === "object") {
		return true;
	}

	/*
		bigint (limited support)
		boolean
		function
		number
		null
		string
		symbol (limited support)
		undefined
	*/

	return false;
}

const mods = {
	1: ({ keys, mBracket, delta, success }) => {
		if (isReferenceable(mBracket)) {
			bracketAdd({
				bracket: mBracket,
				place: keys,
				value: delta[1],
				success: () => {
					success(mBracket);
				}
			});
		}
		else {
			success({
				reference: false,
				_mBracket: delta[1]
			});
		}
	},
	11: ({ keys, mBracket, delta, success }) => {
		const [
			forwardEvo
			// , backwardEvo
		] = delta;

		if (isReferenceable(mBracket)) {
			bracketAdd({
				bracket: mBracket,
				place: keys,
				strandEvo: {
					evo: forwardEvo,
					catalyze: catalyzeStrandEvo
				},
				success: () => {
					success(mBracket);
				}
			});
		}
		else {
			catalyzeStrandEvo({
				strand: mBracket,
				evo: forwardEvo,
				success: ({ mStrand }) => {
					success({
						reference: false,
						_mBracket: mStrand
					});
				}
			});
		}
	},
	2: ({ keys, mBracket, delta, success }) => {
		bracketAdd({
			bracket: mBracket,
			place: keys,
			value: delta[0],
			success: () => {
				success(mBracket);
			}
		});
	},
	3: ({ keys, mBracket, success }) => {
		bracketRemove({
			bracket: mBracket,
			place: keys,
			success: () => {
				success(mBracket)
			}
		});
	},
	4: ({ mBracket, success }) => {
		success(mBracket);
	}
};

const hasOwnProperty = (obj, prop) => {
	return Object.prototype.hasOwnProperty.call(obj, prop);
};

const catalyze = function(macros) {
	const failure = macros.failure || presets.failure;
	const success = macros.success;
	const evo = macros.evo;
	const bracket = macros.bracket;

	let mBracket = mimic(bracket);

	readList({
		list: evo,
		each: function(entry, index, next) {
			const [ signifier, keys, delta ] = entry;

			if (hasOwnProperty(mods, signifier)) {
				mods[ signifier ]({
					keys,
					mBracket,
					delta,
					success: ({
						reference = true,
						_mBracket
					}) => {
						if (!reference) {
							mBracket = _mBracket;
						}

						next();
					}
				});
			}
			else {
				failure(1, {
					evo,
					bracket
				});
			}
		},

		// called automatically if !(list.length >= 1)
		success: function() {
			success({ mBracket });
		}
	});
};

module.exports = catalyze;
