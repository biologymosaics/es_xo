

/*
	bracketRemove({
		bracket: {
			a: true
		},
		place: [ "a" ],
		success: () => {

		}
	});
*/

/*
	Failures:

	1. Unexpected type of partial in place
	2. Prop not found on bracket
	3. Partial of place indicates an array, however no Array was found.
	4. Array doesn't have the specified index
*/

const readList = require("./../list/read.xo");

const presets = {
	failure: (message, details) => {
		throw new Error(JSON.stringify({
			message,
			details
		}));
	}
};

const hasOwnProperty = (obj, prop) => {
	return Object.prototype.hasOwnProperty.call(obj, prop);
};

module.exports = function(macros) {
	const { bracket, place, success } = macros;
	const failure = macros.failure || presets.failure;

	let current = bracket;

	readList({
		list: place,
		each: function(partial, i, next) {
			if (
				typeof partial === "string" ||
				typeof partial === "number"
			) {
				if (hasOwnProperty(current, partial)) {
					if (i === place.length - 1) {
						delete current[ partial ];
					}
					else {
						current = current[ partial ];
					}

					next();
				}
				else {
					failure(2, {
						bracket,
						place
					});
				}
			}
			else if (Array.isArray(partial)) {
				if (Array.isArray(current)) {
					const [ index ] = partial;
					if (current.length <= index) {
						failure(4, {
							place,
							partial,
							bracket
						});
					}
					else {
						if (i === place.length - 1) {
							delete current[ index ];
						}
						else {
							current = current[ index ];
						}
						next();
					}
				}
				else {
					failure(3, {
						place,
						partial,
						bracket
					});
				}
			}
			else {
				failure(1, {
					bracket,
					partial,
					place
				});
			}
		},
		success: function() {
			success();
		}
	});
};
